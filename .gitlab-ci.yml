# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
- test
- lint
- build
- release


# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
cache:
  paths:
    - .cache/pip

# The following variables are used by the included template(s)
.repo-variables:
  variables: &repo-variables
    PYPI_REGISTRY: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi
    PYPI_REGISTRY_USERNAME: gitlab-ci-token
    PYPI_REGISTRY_PASSWORD: ${CI_JOB_TOKEN}

.group-variables:
  variables: &group-variables
    PYPI_REGISTRY: ${CI_API_V4_URL}/groups/${CI_GROUP_ID}/-/packages/pypi # if you want to use group level pypi registry,
    PYPI_REGISTRY_USERNAME: gitlab-ci-token
    PYPI_REGISTRY_PASSWORD: ${CI_JOB_TOKEN}

.install-poetry: &install-poetry
  before_script:
    - pip install poetry
    - poetry config virtualenvs.create false

.python-run: &python-run
  image: python:3.9
  <<: *install-poetry

.rules-anchors:
  rules:
    - if: &main-branch-merge-request
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main" && $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: &develop-branch-merge-request
        $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop" && $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: &release-branch-push
        $CI_COMMIT_BRANCH =~ /^release\/.*/ && $CI_PIPELINE_SOURCE == "push"
      when: always
    - if: &main-branch-push
        $CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "push"
      when: always
    - if: &develop-branch-push
        $CI_COMMIT_BRANCH == "develop" && $CI_PIPELINE_SOURCE == "push"
      when: always
    - if: &tag-push
        $CI_COMMIT_TAG
      when: always

    # when .gitlab-ci.yml is changed, run all stages
    - if: &cicd-pipeline-change
      changes:
        - .gitlab-ci.yml
      when: always

sast:
  stage: test

unit-test:
  <<: *python-run
  stage: test
  script:
    - poetry install
    - nox -s tests

lint:
  <<: *python-run
  stage: lint
  script:
    - poetry install
    - nox -s lint

# Build docs and publish to gitlab pages if on main branch
build-docs:
  image: busybox
  stage: build
  rules:
    - if: *main-branch-push
      when: on_success
    - if: *cicd-pipeline-change
      when: always
    - when: never
  script:
    - echo "The site will be deployed to $CI_PAGES_URL"
  artifacts:
    paths:
      - public

build-package:
  <<: *python-run
  stage: build
  # only run on main branch(merge request) or release/* branches
  rules:
    - if: *main-branch-merge-request
      when: always
    - if: *develop-branch-merge-request
      when: always
    - if: *release-branch-push
      when: always
    - when: never

  script:
    - poetry install
    - poetry build
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - dist/*.whl
      - dist/*.tar.gz

publish-package:
  <<: *python-run
  stage: release
  variables:
    <<: *repo-variables
    # <<: *group-variables # if you want to use group level pypi registry
  rules:
    - if: *tag-push
      when: never
    - if: *main-branch-push
      when: on_success
    - when: never
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - dist/*.whl
      - dist/*.tar.gz
  needs:
    - build-package
  script:
    - pip install twine
    - TWINE_PASSWORD=${PYPI_REGISTRY_PASSWORD} TWINE_USERNAME=${PYPI_REGISTRY_USERNAME} python -m twine upload --repository-url ${PYPI_REGISTRY} dist/* --verbose

release-package:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  # only run on release/* branches and when build-package is successful
  rules:
    - if: *tag-push
      when: never
    - if: *release-branch-push
      when: on_success
    - when: never
  needs:
    - build-package
    - build-docs
  script:
    - echo "release package"
  release:
    tag_name: '$CI_COMMIT_REF_SLUG'
    description: "Release $CI_COMMIT_REF_SLUG"
    tag_message: "$CI_COMMIT_REF_SLUG"

include:
- template: Security/SAST.gitlab-ci.yml
