import nox


@nox.session
def tests(session):
    session.install('pytest', 'pytest-cov', 'pytest-mock', 'toml')
    session.install('-e', '.')
    session.run('pytest', '--cov=package_name', '--cov-report=term-missing', 'tests')
    session.notify('coverage')


@nox.session
def coverage(session):
    session.install('coverage', 'toml')
    session.run('coverage', 'report', '--show-missing', '--fail-under=50')



@nox.session
def lint(session):
    session.install('toml', 'yapf', 'flake8', 'pyproject-flake8')
    session.run('yapf', '--in-place', '--recursive', './package_name')
    session.run('flake8', 'package_name')


@nox.session
def build_docs(session):
    session.install('poetry')
    session.run('poetry', 'install')
    session.run('pdoc', '--output-dir', 'public', '-d', 'google', 'package_name')
